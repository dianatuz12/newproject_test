const {src, dest, series, watch} = require('gulp');
const scss = require('gulp-sass')(require('sass'))
const fileinclude = require('gulp-file-include');
// const imagemin = require('gulp-imagemin');
// const uglify = require('gulp-uglify');
// const pug = require('gulp-pug');
// const rename = require("gulp-rename");
const connect = require('gulp-connect')

const appPath = {
    scss: './app/scss/**/*.scss',
    js: './app/js/*.js',
    img: [
        './app/img/**/*.jpg',
        './app/img/**/*.png',
        './app/img/*.svg'
    ]
}
const destPath = {
    css: './dist/css/',
    js: './dist/js/',
    img: './dist/img/'
}

function scssCompress() {
    return src(appPath.scss)
        .pipe(scss({
            outputStyle: 'compressed'
        }))
        .pipe(dest(destPath.css))
        .pipe(connect.reload())
}

function img() {
    return src(['./app/img/*.jpg',
        './app/img/*.png',
        './app/img/*.svg'
    ])
        .pipe(dest('.app/dist/img'))
}

function html() {
    return src('./app/*.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(dest('./dist/'))
        .pipe(connect.reload()
        );
}

function server() {
    connect.server({
        name: 'Dev App',
        root: 'dist',
        port: 8080,
        livereload: true
    })
}

watch('app/**/*.html', html);
watch(appPath.scss, scssCompress);
watch(appPath.img, {events: 'add'}, img);

exports.default = series(scssCompress, img, html, server)

